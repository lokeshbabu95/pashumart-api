<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;


$route['login'] =  'admin/login/getLoginInfo';
$route['partner'] = 'admin/partner/createpartner';
//member
$route['member/all'] = 'admin/member/getmembers';
$route['member/(:num)'] = 'admin/member/getmemberbyid'; 
$route['member/edit/(:num)'] = 'admin/member/editmember';
$route['member/details/(:num)'] = 'admin/member/getdetails';

//employee
$route['employee'] = 'admin/employee/getemployees';
$route['employee/member/(:num)'] = 'admin/employee/getemployeemember';


//partner
$route['doctors'] = 'partners/doctors/getdoctor';
$route['logistics'] = 'partners/logistics/getlogistics';
$route['food'] = 'partners/food/getfood';
$route['loan'] = 'partners/loan/getloan';
$route['partner/doctor/(:num)'] = 'partners/doctors/getdoctorbyid';
$route['partner/logistics/(:num)'] = 'partners/logistics/getlogisticsbyid';
$route['partner/food/(:num)/(:num)'] = 'partners/food/getfoodbyid';
$route['partner/loan/(:num)/(:num)'] = 'partners/loan/getloanbyid';


//dashboard
$route['dashboard'] = 'admin/dashboard/getdashboarddata';
// $route['dashboard/date'] = 'admin/dashboard/getdashboardbydate';
// $route['dashboard/month'] = 'admin/dashboard/getdashboardbymonth';
$route['dashboard/animal/(:any)'] = 'admin/dashboard/getdashboardbyanimal';
$route['dashboard/state/(:any)'] = 'admin/dashboard/getdashboardbtstate';
$route['dashboard/state/(:any)/(:any)'] = 'admin/dashboard/getdashboardbydistrict';

//ticketing
$route['requestdata'] = 'partners/request/postreqquest';
$route['request/all'] = 'partners/request/getrequest';

//animals
$route['animals/all'] = 'admin/animal/getanimal';
$route['animal/(:num)']  = 'admin/animal/getanimalbyid';
$route['animal/update/(:num)'] = 'admin/animal/editanimal';
$route['animal/health/(:num)'] = 'admin/animal/gethealthbyid';
$route['animal/update/health/(:num)'] = 'admin/animal/edithealth';
$route['animal/image/(:num)'] = 'admin/animal/getimage';
$route['image'] = 'admin/animal/getimage1';
$route['upload/image'] = 'admin/animal/uploadimage';
$route ['retrive/image/(:num)'] = 'admin/animal/retriveimage';



// $route['partner/loan/(:num)'] = 'partners/loan/getloanbyid';
$route['dashboard/date1/(:num)'] = 'admin/dashboard/getdashboardbydate1';
//$route['dashboard/(:num)'] = 'admin/dashboard/getdashboardbystate';

$route['reportby/breed'] = 'admin/report/getreportbybreed';
$route['reportby/state'] = 'admin/report/getreportbystate';
$route['reportby/district'] = 'admin/report/getreportbydistrict';
$route['districts'] = 'admin/report/getdistrict';
 