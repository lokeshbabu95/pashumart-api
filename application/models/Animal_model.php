<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Animal_model extends CI_Model {

    public function get_animal(){
        $query1 = "select a.*,concat(p.first_name,' ',p.last_name) as name,p.district,p.location,ai.body_weight as weight from  animals a join partner p on p.id = a.partner_id
        join additional_information ai on ai.animal_id = a.animal_id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getanimalbyid($Id){
        $query1 = "select a.species,a.breed,a.animal_age,a.gender,a.animal_id,a.approx_cost,a.sold_date,a.reason,ai.*,ad.* from animals a 
        left join animal_insurance ai on ai.animal_id = a.animal_id
        left join additional_information ad on ad.animal_id = a.animal_id
       where a.animal_id =$Id ;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function gethealthbyid($Id){
        $query1 = "select * from health_records where animal_id =$Id ;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getimage($id){
        $query1 = "select * from animal_image where animal_id =$id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getimage1(){
        $query1 = "select * from new_table";
        echo $this->db->last_query();
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
       // print_R($result_data);
        return $result_data;
    }
    public function put ($where = array(), $data = array(), $tableName)
    {
        $this->db->where($where);
        $this->db->update($tableName, $data);

        $data = $this->get($where, $tableName);
        $error = $this->db->error();

        return $response = [
            'data' => $data,
            "last_inserted_id" => "1",
            'error' => $error
        ];
    }
    public function get($where = array(), $tableName)
    {
        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query();

        $error = $this->db->error();
        $data = NULL;
        if($query->num_rows() >= 1)
        {
            $data = $query->result_array();
        }
        return $response = [
            'data' => $data,
            'error' => $error
        ];
    }
    public function getreportbybreed($breed,$start,$end){

        $query1 = "select a.* from animals a where a.species = '$breed'  and  a.sold_date > '$start' 
        AND a.sold_date <= '$end' ORDER BY a.sold_date DESC;";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;

    }
    public function getreportbystate($state,$start,$end){
        
        $query1 = "select a.*,p.* from animals a 
        join logins l on l.id = a.login_id 
        join partner p on p.login_id = l.id
        where p.state = '$state'  and  a.sold_date > '$start' 
        AND a.sold_date <= '$end' ORDER BY a.sold_date DESC;";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getreportbydistrict($state,$district,$start,$end){
        $query1 = "select a.*,p.* from animals a 
        join logins l on l.id = a.login_id 
        join partner p on p.login_id = l.id
        where p.state = '$state' and p.district = '$district' and  a.sold_date > '$start' 
        AND a.sold_date <= '$end' ORDER BY a.sold_date DESC;";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getdistrict($state){
        $query1 = "select DISTINCT p.district as display_name,p.district as value,p.state as state from partner p
        where p.state = '$state' ";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

}
?>