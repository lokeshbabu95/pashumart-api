<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_model extends CI_Model {

    // public function get_partner(){
    //     $query1 = "select l.id as loginid,l.email,l.mobile_number,concat(p.first_name,' ',p.last_name) as name,ur.display_name as user_type,b.* from partner p
    //     join logins l on l.id = p.login_id
    //     left join bank_details b on b.partner_id = p.id
    //     join users_role u on u.login_id = l.id
    //     join users_type ur on ur.id = u.user_type_id where u.user_type_id IN(1,2,3);";
    //     $query = $this->db->query($query1);
    //     $error_result = $this->db->error();
    //     $result_data = $query->result_array();
    //     return $result_data;
    // }
    public function get_doctor(){
        $query1 = "select d.*,l.*,ur.display_name from doctors d
        join logins l on l.id = d.login_id
        join users_role u on u.login_id = l.id
                join users_type ur on ur.id = u.user_type_id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getlogistics(){
        $query1 = "select lo.*,l.* from logistics lo
        join logins l on l.id = lo.login_id
        join users_role u on u.login_id = l.id
                join users_type ur on ur.id = u.user_type_id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getfood(){
        $query1 = "select f.*,l.*,f.id as foodid from food f
        join logins l on l.id = f.login_id
        join users_role u on u.login_id = l.id
                join users_type ur on ur.id = u.user_type_id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getloan(){
        $query1 = "select f.*,l.*,concat(p.first_name,' ',p.last_name) as name from loan f
        join logins l on l.id = f.login_id
        join partner p on p.login_id = l.id
        join users_role u on u.login_id = l.id
                join users_type ur on ur.id = u.user_type_id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    
    public function getdashboardByAnimal(){
        $query1 = "SELECT  species, COUNT(*) AS count
        FROM animals where status = 'sold'
        GROUP BY  species ORDER BY count DESC;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardByanimalmonth($month){
        
        $query1 = "SELECT  species, COUNT(*) AS count
        FROM animals where status = 'sold' and   DATE_FORMAT(sold_date, '%m-%Y')  = '$month'
        GROUP BY  species ORDER BY count DESC;";
        $query = $this->db->query($query1); 
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardByanimalyear($month){
        
        $query1 = "SELECT  species, COUNT(*) AS count
        FROM animals where status = 'sold' and   DATE_FORMAT(sold_date, '%Y')  = $month
        GROUP BY  species ORDER BY count DESC;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardByAnimalweek($newdate,$diffdata){
        $query1 = "SELECT  species, COUNT(*) AS count
        FROM animals  where status = 'sold' and  sold_date > '$newdate' 
        AND sold_date <= '$diffdata' 
        GROUP BY  species ORDER BY count DESC;";
        $query = $this->db->query($query1);
      
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardBystate(){
        $query1 = "select p.state,a.status,count(*) AS Count from partner p
        join animals a on a.partner_id = p.id where a.status = 'sold'
        Group by state ORDER BY Count DESC";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardBystateweek($newdate,$diffdata){
        $query1 = "select p.state,a.status,count(*) AS count from partner p
        join animals a on a.partner_id = p.id where a.status = 'sold' and  a.sold_date > '$newdate' 
        AND a.sold_date <= '$diffdata' 
        Group by state ORDER BY count DESC";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardBystatemonth($month){
        $query1 = "select p.state,a.status,count(*) AS count from partner p
        join animals a on a.partner_id = p.id where a.status = 'sold' and   DATE_FORMAT(sold_date, '%m-%Y')  = '$month' 
        Group by state ORDER BY Count DESC";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardBystateyear($year){
        $query1 = "select p.state,a.status,count(*) AS count from partner p
        join animals a on a.partner_id = p.id where a.status = 'sold' and   DATE_FORMAT(sold_date, '%Y')  = $year
        Group by state ORDER BY Count DESC";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getdashboardbydate($newdate){
        $query1 = " select date_created,Count(*) As count from logins where date_created = '$newdate'";
        // echo $this->db->last_query();
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardbydate1($newdate,$diffdata){
        $query1 = " select date_created,Count(*) As count from logins WHERE date_created > '$newdate' 
        AND date_created <= '$diffdata' ";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function addrequest($data){
        // unset($data->is_default);
        $insert_query = $this->db->insert('ticketing', $data);
    
        $error = $this->db->error();
        $lastInsertedId = 0;
        if ($insert_query)
        {
            $lastInsertedId = $this->db->insert_id();
        }
    
        $where = ['id' => $lastInsertedId];
       
    
        return [
            
            "last_inserted_id" => $lastInsertedId,
            "error" => $error
        ];
    }
    public function getrequest(){
        $query1 = " select * from ticketing ORDER BY date DESC ";
        // echo $this->db->last_query();
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdoctorbyid($id){
        $query1 = "select d.* from doctors d  join logins l on l.id = d.login_id where l.id =$id";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getlogisticsbyid($Id){
        $query1 = "select lo.*,l.* from logistics lo  join logins l on l.id = lo.login_id where l.id =$Id";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getfoodbyid($Id,$foodid){

        $query1 = "select lo.*,l.mobile_number,l.email from food lo  join logins l on l.id = lo.login_id where lo.login_id =$Id and lo.id = $foodid";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getdoctortreatments($id){
        $query1 = "select h.*,a.species from health_records h join animals a on a.animal_id = h.animal_id where h.treated_by = $id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    // public function getmemberbyid($id){
    //     $query1 = "select a.*,concat(p.first_name,' ',p.last_name) as name from  animals a join partner p on p.id = a.partner_id join logins l on l.id = p.login_id 
    //     where l.id = $id ;";
    //     $query = $this->db->query($query1);
    //     $error_result = $this->db->error();
    //     $result_data = $query->result_array();
    //     return $result_data;
    // }

    public function getloanbyid($id,$loanid){
        $query1 = "select lo.*,concat(p.first_name,' ',p.last_name) as name,l.mobile_number,l.email from loan lo join logins l  on l.id = lo.login_id  join partner p on p.login_id = lo.login_id
         where lo.login_id = $id  and lo.id = $loanid;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
}
?>