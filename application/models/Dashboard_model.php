<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function getdashboardBystateyear($state,$year){
        $query1 = "select p.district,a.status,count(*) AS count from partner p
        join animals a on a.partner_id = p.id where p.state = '$state' and  a.status = 'sold' and   DATE_FORMAT(sold_date, '%Y')  = $year
        Group by district ORDER BY Count DESC;";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getdashboardBystatemonth($state,$month){
        $query1 = "select p.district,a.status,count(*) AS count from partner p
        join animals a on a.partner_id = p.id where p.state = '$state' and  a.status = 'sold' and   DATE_FORMAT(sold_date, '%m-%Y')  = $month
        Group by district ORDER BY Count DESC;";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
    public function getdashboardBystateweek($newdate,$diffdata){
        $query1 = "select p.district,a.status,count(*) AS count from partner p
        join animals a on a.partner_id = p.id where p.state = '$state' and  a.status = 'sold' and   and  a.sold_date > '$newdate' 
        AND a.sold_date <= '$diffdata'
        Group by district ORDER BY Count DESC;";
        
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
}
?>