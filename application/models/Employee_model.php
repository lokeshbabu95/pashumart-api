<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

    public function get_employee(){
        $query1 = "select l.*,concat(e.first_name,' ',e.last_name) as name,e.* from  employees e join logins l  on l.id = e.login_id
        join users_role u on u.login_id = l.id
               join users_type ur on ur.id = u.user_type_id ";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getemployeemember($id){
        $query1 = "select l.id as loginid,l.email,l.mobile_number,concat(p.first_name,' ',p.last_name) as name,ur.display_name as user_type,b.*,
        (select count(*)  from animals where animals.login_id = l.id) as count,
        (select count(*) as count from animals where animals.login_id = l.id and animals.status = 'sold') as sale from partner p
         
        join logins l on l.id = p.login_id
        left join bank_details b on b.partner_id = p.id
        join users_role u on u.login_id = l.id
        join users_type ur on ur.id = u.user_type_id where l.employee_id = $id ";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
//     select l.email,(select count(*)  from animals where animals.login_id = l.id) as count,
// (select count(*) as count from animals where animals.login_id = l.id and animals.status = 'sold') as sale 

// from logins l where l.employee_id =
    public function getemployeeanimalcount($id){
        $query1 = "select (select count(*)  from animals where animals.login_id = l.id) as count,(select count(*) from logins ls where ls.employee_id = $id) as total_member,
        (select count(*) as count from animals where animals.login_id = l.id and animals.status = 'sold') as sale 
        from logins l where l.employee_id = $id ";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }
}
?>