<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {

    public function getmember(){
        $query1 = "select l.id as loginid,l.email,l.mobile_number,concat(p.first_name,' ',p.last_name) as name,ur.display_name as user_type,b.* ,
        (select count(*)  from animals where animals.login_id = l.id) as count,
        (select count(*) as count from animals where animals.login_id = l.id and animals.status = 'sold') as sale from partner p
        join logins l on l.id = p.login_id
        left join bank_details b on b.partner_id = p.id
        join users_role u on u.login_id = l.id
        join users_type ur on ur.id = u.user_type_id where u.user_type_id IN(1,2,3);";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getmemberbyid($id){
        $query1 = "select a.*,concat(p.first_name,' ',p.last_name) as name,p.district,p.location,ai.body_weight as weight from  animals a 
        join partner p on p.id = a.partner_id join logins l on l.id = p.login_id 
        join additional_information ai on ai.animal_id = a.animal_id
        where l.id = $id ;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }

    public function getmemberdetails($id){
        $query1 = "select * from logins l left join partner p on p.login_id = l.id left join bank_details b on b.login_id = l.id where l.id = $id;";
        $query = $this->db->query($query1);
        $error_result = $this->db->error();
        $result_data = $query->result_array();
        return $result_data;
    }


    public function put ($where = array(), $data = array(), $tableName)
    {
        $this->db->where($where);
        $this->db->update($tableName, $data);

        $data = $this->get($where, $tableName);
        $error = $this->db->error();

        return $response = [
            'data' => $data,
            "last_inserted_id" => "1",
            'error' => $error
        ];
    }
    public function get($where = array(), $tableName)
    {
        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query();

        $error = $this->db->error();
        $data = NULL;
        if($query->num_rows() >= 1)
        {
            $data = $query->result_array();
        }
        return $response = [
            'data' => $data,
            'error' => $error
        ];
    }
}
?>