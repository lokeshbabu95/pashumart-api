<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Doctors extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('partner_model');
    }

    

     public function getdoctor_get(){
        $data = $this->partner_model->get_doctor();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     public function getdoctorbyid_get()
     {  $doctorid =  $this->uri->segment(3);
        //$this->uri->segment(3);
         $data = $this->partner_model->getdoctorbyid($doctorid);
         $treatment = $this->partner_model->getdoctortreatments($doctorid);

         $response['doctor_data'] =  $data;
         $response['treatments'] = $treatment;
         $this->response($response,REST_CONTROLLER::HTTP_OK);

     }

}
?>