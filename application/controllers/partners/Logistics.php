<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Logistics extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('partner_model');
    }

    

     public function getlogistics_get(){
        $data = $this->partner_model->getlogistics();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     public function getlogisticsbyid_get(){
        $logisticsid =  $this->uri->segment(3);
        //$this->uri->segment(3);
         $data = $this->partner_model->getlogisticsbyid ($logisticsid);
         $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

}
?>