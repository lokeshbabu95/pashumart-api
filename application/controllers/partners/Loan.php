<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Loan extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('partner_model');
    }

    

     public function getloan_get(){
        $data = $this->partner_model->getloan();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function getloanbyid_get(){
         $loginid = $this->uri->segment(3);
         $loanid = $this->uri->segment(4);
         $data = $this->partner_model->getloanbyid($loginid,$loanid);
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

}
?>