<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Food extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('partner_model');
    }

    

     public function getfood_get(){
        $data = $this->partner_model->getfood();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function getfoodbyid_get(){
        $loginid =  $this->uri->segment(3);
        $foodid = $this->uri->segment(4);
        //$this->uri->segment(3);
         $data = $this->partner_model->getfoodbyid($loginid,$foodid);
         $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
}
?>