<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Request extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('partner_model');
    }

    

     public function getrequest_get(){
        $data = $this->partner_model->getrequest();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     public function postreqquest_post(){
         $postvariables = $this->post();
         $upload = $this->partner_model->addrequest($postvariables);
        if($upload['last_inserted_id']){
            $response['data' ] = 'sucess';
            $this->response($response,REST_CONTROLLER::HTTP_OK);
        }
     }


}
?>