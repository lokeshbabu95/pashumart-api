<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Dashboard extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(arraY('partner_model','dashboard_model'));
    }

    // public function getdashboardbystate_get(){
    //     $id = $this->uri->segment(2);
    //      $date = date('Y-m-j'); 
    //          $a =7;
    //         $newdate = strtotime ( '-'.($id * $a).'day' , strtotime ( $date ) ) ;
    //      $newdate = date ( 'Y-m-j' , $newdate );
       
    //     $data_by_state = $this->partner_model->getdashboardBystate1($newdate,$date);
    //     $b = 0;
    //     foreach ($data_by_state as $key ) {
    //         $b +=$key['Count'];
    //    }
    //    foreach ($data_by_state  as $key1 => $value){
    //     $data_by_state[$key1]['percentage'] = round(($value['Count']/$b)*100, 1);
    //     }
    //     $response['data']['sale_by_state'] = $data_by_state;
    //     $response['data']['total_sale_by_state'] = $b;
    //      $this->response($response,REST_CONTROLLER::HTTP_OK);
    // }

    public function getdashboardbtstate_get(){
        $id = $this->uri->segment(3);
        switch($id){
            case 'week':
            $date = date('Y-m-j'); 
             $a =7;
            $newdate = strtotime ( '-'.($a ).'day' , strtotime ( $date ) ) ;
            $newdate = date ( 'Y-m-j' , $newdate );
            $data_by_state = $this->partner_model->getdashboardBystateweek($newdate,$date);
            break;
            case 'month':
            
            $data_by_state = $this->partner_model->getdashboardBystatemonth(date('m-Y'));
            break;
            case 'year':
            $data_by_state = $this->partner_model->getdashboardBystateyear(date('Y'));
            break;
        }
        $b = 0;
        foreach ($data_by_state as $key ) {
            $b +=$key['count'];
       }
       foreach ($data_by_state  as $key1 => $value){
        $data_by_state[$key1]['percentage'] = round(($value['count']/$b)*100, 1);
        }
        $response['data']['sale_by_state'] = $data_by_state;
        $response['data']['total_sale_by_state'] = $b;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
    }


    public function getdashboardbyanimal_get(){
        $id = $this->uri->segment(3);
        switch($id){
            case 'week':
            $date = date('Y-m-j'); 
             $a =7;
            $newdate = strtotime ( '-'.($a ).'day' , strtotime ( $date ) ) ;
            $newdate = date ( 'Y-m-j' , $newdate );
               
            $data_by_state = $this->partner_model->getdashboardByAnimalweek($newdate,$date);
            break;
            case 'month':
            
            $data_by_state = $this->partner_model->getdashboardByanimalmonth(date('m-Y'));
            break;
            case 'year':
            $data_by_state = $this->partner_model->getdashboardByanimalyear(date('Y'));
            break;
        }
        $b = 0;
        foreach ($data_by_state as $key ) {
            $b +=$key['count'];
       }
       foreach ($data_by_state  as $key1 => $value){
        $data_by_state[$key1]['percentage'] = round(($value['count']/$b)*100, 1);
        }
        $response['data']['sale_by_animal'] = $data_by_state;
        $response['data']['total_sale_by_animal'] = $b;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
    }

     public function getdashboarddata_get(){
        
        $a = 0;
        $data_by_animal = $this->partner_model->getdashboardByAnimal();
        $data_by_state = $this->partner_model->getdashboardBystate();
        foreach ($data_by_animal as $key ) {
             $a +=$key['count'];
        }
        foreach($data_by_animal as $key1 => $value){
             $data_by_animal[$key1]['percentage'] = round(($value['count']/$a)*100, 1);
        }
       $b = 0;
        foreach ($data_by_state as $key ) {
            $b +=$key['Count'];
       }
        foreach ($data_by_state  as $key1 => $value){
            $data_by_state[$key1]['percentage'] = round(($value['Count']/$b)*100, 1);
        }
        $response['data']['sale_by_type'] =   $data_by_animal;

        $response['data']['sale_by_state'] = $data_by_state;
        $response['data']['total_sale_by_type'] = $a;
        $response['data']['total_sale_by_state'] = $b;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }



        public function getdashboardbydate_get(){
            $dashboarddata = array();
            // $id = $this->uri->segement(3);
           //echo $id;
        $date = date('Y-m-j');

        
        for ($i=0; $i < 7 ; $i++) { 
            $newdate = strtotime ( '-'.$i.'day' , strtotime ( $date ) ) ;
        $newdate = date ( 'Y-m-j' , $newdate );
        // echo $newdate . "<br>";
        $data = $this->partner_model->getdashboardbydate($newdate);
        $data[0]['current_date'] = $newdate;
        $dashboarddata[] = $data ;
          
        }
        $response = $dashboarddata;
        $this->response($response,REST_CONTROLLER::HTTP_OK);
        }



        public function getdashboardbydate1_get(){
            $dashboarddata = array();
            $id = $this->uri->segment(3);
            //  echo "Today is " . date("Y-m-d") . "<br>";
        $date = date('Y-m-j');
        // echo $date."<br>";

        
        for ($i=1; $i <=$id ; $i++) { 
            $a =7;
            $newdate = strtotime ( '-'.($i * $a).'day' , strtotime ( $date ) ) ;
        $newdate = date ( 'Y-m-j' , $newdate );
        $diffdata = strtotime ( '+'. $a.'day' , strtotime ( $newdate  ) ) ;
        $diffdata = date('Y-m-j' , $diffdata );
        $data = $this->partner_model->getdashboardbydate1($newdate,$diffdata);
        $data[0]['current_date'] = $diffdata;
        $dashboarddata[] = $data ;
          
        }
        $response = $dashboarddata;
        $this->response($response,REST_CONTROLLER::HTTP_OK);
        }


        public function getdashboardbymonth_get(){
            $date = date('Y-m-j');
            echo date('m-Y');
            echo $newdate = date("Y-m-j", strtotime("-1 months"));
            $month_name =  ucfirst(strftime("%B", strtotime($date)));
            echo $month_name;
           // $newdate = strtotime('-1');
        }

        public function getdashboardbydistrict_get(){
            $state = $this->uri->segment(3);
            $state = str_replace("%20",' ',$state);
            $id = $this->uri->segment(4);
            switch($id){
                case 'week':
                $date = date('Y-m-j'); 
                 $a =7;
                $newdate = strtotime ( '-'.($a ).'day' , strtotime ( $date ) ) ;
                $newdate = date ( 'Y-m-j' , $newdate );
                $data_by_state = $this->dashboard_model->getdashboardBystateweek($newdate,$date);
                break;
                case 'month':
                
                $data_by_state = $this->dashboard_model->getdashboardBystatemonth($state,date('m-Y'));
                break;
                case 'year':
                $data_by_state = $this->dashboard_model->getdashboardBystateyear($state,date('Y'));
                break;
            }
            $b = 0;
            foreach ($data_by_state as $key ) {
                $b +=$key['count'];
           }
           foreach ($data_by_state  as $key1 => $value){
            $data_by_state[$key1]['percentage'] = round(($value['count']/$b)*100, 1);
            }
            $response['data']['sale_by_state'] = $data_by_state;
            $response['data']['total_sale_by_state'] = $b;
             $this->response($response,REST_CONTROLLER::HTTP_OK);
        }




        
}
?>