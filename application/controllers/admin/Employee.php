<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Employee extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('employee_model');
    }

    public function createemployee_post(){
        $postVariables = $this->post();

     }

     public function getemployees_get(){
        $data = $this->employee_model->get_employee();
        foreach ($data as $key => $value) {
           $data1 = $this->getemployeemember($value['login_id']);
           $data[$key]['total_animal'] = $data1['total_animal'];
           $data[$key]['total_sold'] = $data1['total_sold'];
           $data[$key]['total_member'] = $data1['total_member'];
          //print_r($value);
        } 
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function getemployeemember($Id){
         $a = 0;
         $b= 0;
         $data1 = $this->employee_model->getemployeeanimalcount($Id);
         foreach ($data1 as $key => $value) {
             $a = $value['count']+$a; 
             $b = $value['sale']+$b;
         }
         $response = array(
             "total_animal"=>$a,
             "total_sold"=>$b,
             "total_member"=>$data1 ? $data1[0]['total_member'] : 0
         );
         return $response;
        
        //  $memberid = $this->uri->segment(3);
        //  $data = $this->employee_model->getemployeemember($memberid);
        //  $response['data'] =  $data;
        //  $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function getemployeemember_get(){
          $memberid = $this->uri->segment(3);
         $data = $this->employee_model->getemployeemember($memberid);
         $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

    
}
?>