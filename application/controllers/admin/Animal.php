<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Animal extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('animal_model');
    }

    public function createanimal_post(){
        $postVariables = $this->post();

     }

     public function getanimal_get(){
        $data = $this->animal_model->get_animal();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     public function getanimalbyid_get(){
        $animalId = $this->uri->segment(2);
         $data = $this->animal_model->getanimalbyid($animalId);
         $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);

     }
     public function gethealthbyid_get(){
        $animalId = $this->uri->segment(3);
        $data = $this->animal_model->gethealthbyid($animalId);
        $response['data'] =  $data;
        $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     public function postimage_post(){
         $postVariables = $this->post();
         print_r($postVariables);
     }
     public function getimage_get(){
        $animalId = $this->uri->segment(3);
         $data = $data = $this->animal_model->getimage($animalId);
         $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     public function getimage1_get(){
        
         $data = $data = $this->animal_model->getimage1();
         //$response['data'] =  $data;
         print_r(fbsql_read_blob($data[1]['image']));
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function editanimal_post(){
        $animalId = $this->uri->segment(3);
         $postVariables = $this->post();
         $animaldata  = array(
                 "species" => $postVariables['species'],
             "breed" =>$postVariables['breed'],
             "animal_age" =>$postVariables['animal_age'],
             "gender"=>$postVariables['gender'],
            //  "status"=>$postVariables['status'],
             "approx_cost"=>$postVariables['approx_cost'],
             "sold_date"=>$postVariables['sold_date'],
             "reason"=>$postVariables['reason']
         );
         $insurance= array(
             "insurance_company" =>$postVariables['insurance_company'],
             "insurance_cost"=>$postVariables['insurance_cost'],
             "insurance_no"=>$postVariables['insurance_no'],
             "starting_date"=>$postVariables['starting_date'],
             "ending_date"=>$postVariables['ending_date']
         );
         $additional = array(
            "body_weight" => $postVariables['body_weight'],
            "color"=>$postVariables['color'],
            "teeth_count"=>$postVariables['teeth_count'],
            "horn_distance"=>$postVariables['horn_distance'],
            "no_of_rings"=>$postVariables['no_of_rings'],
            "cost"=>$postVariables['cost'],
            "date_of_purchase"=>$postVariables['date_of_purchase'],
            "source"=>$postVariables['source'],
            "artificial_inseminated"=>$postVariables['artificial_inseminated'],
            "fathered_by"=>$postVariables['fathered_by'],
            "mother_by"=>$postVariables['mother_by'],
            "no_of_calving"=>$postVariables['no_of_calving'],
            "calving_option"=>$postVariables['calving_option'],
            "calving"=>$postVariables['calving'],
            "sire"=>$postVariables['sire'],
            "inseminator"=>$postVariables['inseminator'],
            "calf_sex"=>$postVariables['calf_sex'],
            "milking"=>$postVariables['milking'],
            "total_milk"=>$postVariables['total_milk'],
            "heat_sequence"=>$postVariables['heat_sequence'],
            "pregent_option"=>$postVariables['pregent_option']
         );

         $wherecondition = array("animal_id"=>$animalId);
         $updated = $this->animal_model->put( $wherecondition,$animaldata,'animals');

          $insurancedata = $this->animal_model->put($wherecondition,$insurance,'animal_insurance');
          $additionaldata = $this->animal_model->put($wherecondition,$additional,'additional_information');
         $response['data'] =  $updated;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
        
     }

     public function edithealth_post(){
         $postVariables = $this->post();
        
     $animalId = $this->uri->segment(4);
         $healthdata = array(
            "disease"=>$postVariables['disease'],
            "vaccine"=>$postVariables['vaccine'],
            "date"=>$postVariables['date'],
            "route"=>$postVariables['route'],
            "doses"=>$postVariables['doses'],
            "treated_on"=>$postVariables['treated_on']

        );
        $wherecondition = array("animal_id"=>$animalId,"id"=> $postVariables['id']);
        $health = $this->animal_model->put($wherecondition,$healthdata,'health_records');
        $response['data'] = $health;
        $this->response($response,REST_CONTROLLER::HTTP_OK);   

     }

     public function uploadimage_post(){
        $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'] . "/uploads/";
         $config['allowed_types']        = '*';
       // $config['max_size']             = 500000;
        echo $_SERVER['DOCUMENT_ROOT'];

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('userfile')){

            $error = array('error'=>$this->upload->display_errors());
            print_r($error);
        }else{
            $data  = array('upload_data'=>$this->upload->data());
            print_r($data);
            $file_path =  $data['upload_data']['full_path'];
            echo $file_path;
        }
//       $data1 =   $this->upload->do_upload('userfile');
//      // echo $data1;
//         $data = array('upload_data' => $this->upload->data());
//                 $file_path =  $data['upload_data']['full_path'];
                
//                //$this->load->view('upload_success',$data);
// print_r($data);
//                 echo $file_path;

//                 $this->load->library('upload', $config); //Load the upload CI library
// if (!$this->upload->do_upload('userfile')){
//     $uploadError = array('upload_error' => $this->upload->display_errors()); 
//     $this->set_flashdata('uploadError', $uploadError, $urlYouWantToReturn); //If for some reason the    upload could not be done, returns the error in a flashdata and redirect to the page you specify in $urlYouWantToReturn
//     echo"iam here";exit;
// }
     }



     public function retriveimage_get(){
        $animalId = $this->uri->segment(2);
     }
}
?>