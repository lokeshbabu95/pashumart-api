<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Partner extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('partner_model','animal_model'));
    }

    public function createpartner_post(){
        $postVariables = $this->post();

     }

    //  public function getpartner_get(){
    //     $data = $this->partner_model->get_partner();
    //     $response['data'] =  $data;
    //      $this->response($response,REST_CONTROLLER::HTTP_OK);
    //  }

     public function getdoctor_get(){
        $data = $this->partner_model->get_partner();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

    //  public function getmemberbyid_get(){
    //      $memberId = $this->uri->segment(3);
    //      $data = $this->partner_model->getmemberbyid($memberId);
    //     $response['data'] =  $data;
    //      $this->response($response,REST_CONTROLLER::HTTP_OK);
    //  }

     public function editpartner_post(){
        $postVariables = $this->post();
        $wherecondition  = array("id"=>$postVariables['login_id']);
        $data = array(
            "mobile_number"=>$postVariables['mobile_number'],
            "email" => $postVariables['email']
        );
        $data = $this->animal_model->changepassword($wherecondition,$data,'logins');
        $response['data'] = $data;
        $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function changepartnerpassword_post(){
         $postVariables = $this->post();
         $wherecondition  = array("id"=>$postVariables['login_id']);
         $data = array(
             "password"=>$postVariables['password']
         );
         $data = $this->animal_model->changepassword($wherecondition,$data,'logins');
         $response['data'] = $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }
     

}
?>