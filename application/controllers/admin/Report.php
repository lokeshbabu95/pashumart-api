<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Report extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('partner_model','animal_model'));
    }

    public function getreportbybreed_post(){
        $postvariables = $this->post();
        $breed = $postvariables['breed'];
        $start = $postvariables['start_date'];
        $end = $postvariables['end_date'];
        $total_cost = 0;
        $data = $this->animal_model->getreportbybreed($breed,$start,$end);
        foreach ($data as $key) {
            $total_cost = $total_cost + $key['approx_cost'];
           
        }
        
        $response['data'] =  $data;
        $response['total_cost'] = $total_cost;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
    }

    public function getreportbystate_post(){
        $postvariables = $this->post();
        $state = $postvariables['state'];
        $start = $postvariables['start_date'];
        $end = $postvariables['end_date'];
        $total_cost = 0;
        $data = $this->animal_model->getreportbystate($state,$start,$end);
            foreach ($data as $key) {
                $total_cost = $total_cost + $key['approx_cost'];
               
            }
            
            $response['data'] =  $data;
            $response['total_cost'] = $total_cost;
             $this->response($response,REST_CONTROLLER::HTTP_OK);
            }

     public function getreportbydistrict_post(){
         $postvariables = $this->post();
         $state = $postvariables['state'];
         $district = $postvariables['district'];
         $start = $postvariables['start_date'];
         $end = $postvariables['end_date'];
         $total_cost = 0;
         $data = $this->animal_model->getreportbydistrict($state,$district,$start,$end);

         foreach ($data as $key) {
            $total_cost = $total_cost + $key['approx_cost'];
           
        }
        
        $response['data'] =  $data;
        $response['total_cost'] = $total_cost;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }   
     public function getdistrict_post(){
         $postvariables = $this->post();
         $state = $postvariables['state'];
         $data = $this->animal_model->getdistrict($state);
         $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }    
}
?>