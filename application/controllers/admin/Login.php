<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Login extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('login_model');
    }

    public function getLoginInfo_post(){
        $postVariables = $this->post();
        if(!isset($postVariables['username']) || !$postVariables['username'])
        {
            $error = array(
                "error_code" => "PARAMETER_MISSING",
                "message" => "Username Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        if(!isset($postVariables['password']) || !$postVariables['password'])
        {
            $error = array(
                "error_code" => "PARAMETER_MISSING",
                "message" => "Password Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userName = $postVariables['username'];
        $password = md5($postVariables['password']);
               
        $userObj = $this->login_model->login($userName, $password);
        if(!$userObj)
        {
            $error = array(
                "error_code" => "INVALID_INPUT",
                "message" => "EmailId Or Password Is Invalid."
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
            $this->response($userObj, REST_Controller::HTTP_OK);


    }
}
?>