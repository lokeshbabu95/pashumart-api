<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Member extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('member_model');
    }

    public function getmembers_get(){
        $data = $this->member_model->getmember();
        $response['data'] =  $data;
         $this->response($response,REST_CONTROLLER::HTTP_OK);
     }

     public function getmemberbyid_get(){
        $memberId = $this->uri->segment(2);
        $data = $this->member_model->getmemberbyid($memberId);
       $response['data'] =  $data;
        $this->response($response,REST_CONTROLLER::HTTP_OK);
    }

    public function getdetails_get(){
        $memberid = $this->uri->segment(3);
        $data = $this->member_model->getmemberdetails($memberid);
        $response['data'] = $data;
        $this->response($response,REST_CONTROLLER::HTTP_OK);
    }

    public function editmember_post(){
        $postvariables = $this->post();
        $memberid = $this->uri->segment(3);
        $login = array(
            "email"=>$postvariables['email'],
            "password"=>$postvariables['password'],
            "mobile_number"=>$postvariables['mobile_number']
        );

        $partnersdata = array(
            "first_name"=>$postvariables['first_name'],
            "last_name"=>$postvariables['last_name'],
            "age"=>$postvariables['age'],
            "gender"=>$postvariables['gender'],
            "state"=>$postvariables['state'], 
            "district"=>$postvariables['district'],
            "location"=>$postvariables['location'],
            "address"=>$postvariables['address'],
            "pincode"=>$postvariables['pincode'],
            "aadhar_no"=>$postvariables['aadhar_no'],
            
            
        );      

        $bank = array(
            "account_number"=>$postvariables['account_number'],
            "account_name"=>$postvariables['account_name'],
            "bank_name"=>$postvariables['bank_name'],
            "ifsc_code"=>$postvariables['ifsc_code']
        );
       
        $wherecondition = array("id"=>$memberid);
        $logindata = $this->member_model->put($wherecondition,$login,'logins');
        $wherecondition1 = array("login_id"=>$memberid);
        $partneradata =  $this->member_model->put($wherecondition1, $partnersdata,'partner');
        $bankdata =  $this->member_model->put($wherecondition1, $bank,'bank_details');

        $response['data'] = 'sucess';
        $this->response($response,REST_CONTROLLER::HTTP_OK);
    }
}
?>